# CoSApp - Collaborative System Approach

The primary goal of **CoSApp** is to help technical departments in the design of complex systems.
To do so, the framework allows the simulation of various systems representing the different parts of the final product in a common environment.
The benefit is the ability for each subsystem team to carry out design study with a direct feedback at product level.

The main features are:

### Butterfly effect

Couple your simulation models with CoSApp to get immediate impact on main product variables and iterate to converge on a better design.

### Design guidance

All systems can share design parameters associated with an acceptable range.
You can take advantage of those limited degrees of freedom without fear of breaking your collaborators' work.

### Flexible simulation workflows

CoSApp solvers can be combined into versatile, customized workflows that fit specific simulation intents.

Have a look at the [introduction](https://cosapp.readthedocs.io/en/stable/tutorials/00-Introduction.html#), containing many tutorials!

This code is the property of Safran SA.
It uses code coming from various open-source projects (see [LICENSE](https://gitlab.com/cosapp/cosapp/blob/master/LICENSE.rst) file).

# Citing

If you use CoSApp, please cite us!

Lac *et al.* (2024), [CoSApp: a Python library to create, simulate and design complex systems](https://doi.org/10.21105/joss.06292), Journal of Open Source Software 9(94), 6292. 

BibTeX entry:

```bibtex
@article{Lac.etal:joss2024,
    author={Étienne Lac and Guy {De Spiegeleer} and Adrien Delsalle and Frédéric Collonval and Duc-Trung Lê and Mathias Malandain},
    title={CoSApp: a Python library to create, simulate and design complex systems},
    journal={Journal of Open Source Software},
    year={2024},
    volume={9},
    number={94},
    pages={6292},
    doi={10.21105/joss.06292},
    publisher={The Open Journal}
}
```

# Try it now!

Run a Jupyter Lab instance with binder to try out CoSApp features through examples.

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/cosapp%2Fcosapp/master?urlpath=lab/tree/docs/tutorials)
[![lite-badge](https://jupyterlite.rtfd.io/en/latest/_static/badge.svg)]( https://cosapp.gitlab.io/cosapp/)
