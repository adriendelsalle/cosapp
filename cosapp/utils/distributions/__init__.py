"""Module defining fundamental random variable distributions."""
from .distribution import Distribution
from .normal import Normal
from .triangular import Triangular
from .uniform import Uniform
