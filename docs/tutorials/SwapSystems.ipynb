{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![CoSAppLogo](images/cosapp.svg) **CoSApp** examples\n",
    "\n",
    "# Replacing a sub-system dynamically\n",
    "\n",
    "Utility function `cosapp.utils.swap_system` allows one to replace on the fly an existing sub-system by another `System` instance."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from cosapp.utils import swap_system\n",
    "\n",
    "help(swap_system)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Example"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from cosapp.base import System\n",
    "from cosapp.drivers import NonLinearSolver\n",
    "\n",
    "\n",
    "class NominalComponent(System):\n",
    "    def setup(self):\n",
    "        self.add_inward('a', 1.0)\n",
    "        self.add_inward('x', 1.0)\n",
    "        self.add_outward('y', 0.0)\n",
    "\n",
    "    def compute(self) -> None:\n",
    "        self.y = self.a * self.x**2 - 1\n",
    "\n",
    "\n",
    "class DegradedComponent(System):\n",
    "    def setup(self):\n",
    "        self.add_inward('a', 1.0)\n",
    "        self.add_inward('x', 1.0)\n",
    "        self.add_outward('y', 0.0)\n",
    "\n",
    "    def compute(self) -> None:\n",
    "        self.y = self.x - self.a\n",
    "\n",
    "\n",
    "class CompositeSystem(System):\n",
    "\n",
    "    def setup(self):\n",
    "        a = self.add_child(NominalComponent('a'), pulling='x')\n",
    "        b = self.add_child(NominalComponent('b'), pulling='y')\n",
    "\n",
    "        self.connect(a, b, {'y': 'x'})  # a.y -> b.x\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from cosapp.utils import swap_system\n",
    "\n",
    "head = CompositeSystem('head')\n",
    "\n",
    "solver = head.add_driver(NonLinearSolver('solver'))\n",
    "solver.add_unknown('x', max_abs_step=0.25).add_equation('y == 0')\n",
    "\n",
    "head.run_drivers()\n",
    "\n",
    "print(\n",
    "    \"Original config:\\n\",\n",
    "    solver.problem,\n",
    "    sep=\"\\n\",\n",
    ")\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next, we swap `head.a` with a newly created system of type `DegradedComponent`, and retrieve the original sub-system as `original_a`.\n",
    "After the replacement, `original_a` is a parentless, stand-alone system."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "original_a = swap_system(head.a, DegradedComponent('a'))\n",
    "\n",
    "# Checks\n",
    "print(\n",
    "    f\"{head.a.parent = }\",\n",
    "    f\"{original_a.parent = }\",\n",
    "    f\"{type(head.a) = }\",\n",
    "    sep=\"\\n\",\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the process, existing connectors within the parent system are maintained:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "head.connectors()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If we rerun the model, we can see that the mathematical problem is maintained. However, the obtained solution differs from the previous one, since the overall behaviour of system `head` has changed."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Re-run; \n",
    "head.run_drivers()\n",
    "\n",
    "print(\n",
    "    \"Modified config:\\n\",\n",
    "    solver.problem,\n",
    "    sep=\"\\n\",\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can revert to the original configuration, by re-swapping current `head.a` with previously stored object `original_a`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Revert to original sub-system\n",
    "swap_system(head.a, original_a)\n",
    "\n",
    "head.run_drivers()\n",
    "\n",
    "print(\n",
    "    \"Recovered config:\\n\",\n",
    "    solver.problem,\n",
    "    sep=\"\\n\",\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Function `swap_system` can be useful in the context of an event-driven transition, for instance (see tutorial on [discrete events](HybridSimulations.ipynb)):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from cosapp.base import System\n",
    "from cosapp.utils import swap_system\n",
    "\n",
    "\n",
    "class ThreasholdSystem(System):\n",
    "\n",
    "    def setup(self):\n",
    "        a = self.add_child(NominalComponent('a'), pulling='x')\n",
    "        b = self.add_child(NominalComponent('b'), pulling='y')\n",
    "\n",
    "        self.connect(a, b, {'y': 'x'})\n",
    "\n",
    "        self.add_inward('y_max', 3.14)\n",
    "        self.add_event('failure', trigger=\"y > y_max\")\n",
    "        self.add_event('recovery', trigger=\"y < y_max\")\n",
    "\n",
    "    def transition(self):\n",
    "        if self.failure.present:\n",
    "            swap_system(self.a, DegradedComponent('a'))\n",
    "        \n",
    "        if self.recovery.present:\n",
    "            swap_system(self.a, NominalComponent('a'))\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Ground rules"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To avoid undesired side-effects, the substitute system (second argument) must not be part of an existing system tree (*i.e.* its parent should be `None`):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "head1 = CompositeSystem('head1')\n",
    "head2 = CompositeSystem('head2')\n",
    "\n",
    "def print_exception(error: Exception):\n",
    "    print(f\"{type(error).__name__}: {error!s}\")\n",
    "\n",
    "try:\n",
    "    swap_system(head1.a, head2.b)\n",
    "\n",
    "except Exception as error:\n",
    "    print_exception(error)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Oppositely, the swapped system (first argument) must be the child of a higher-level system:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "try:\n",
    "    swap_system(head1, CompositeSystem('new'))\n",
    "\n",
    "except Exception as error:\n",
    "    print_exception(error)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.12"
  },
  "orig_nbformat": 4
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
